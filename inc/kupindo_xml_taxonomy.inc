<?php

/**
 * @file
 * Kupindo XML taxonomy operations.
 */

/**
 * Creates the Kupindo catalog and taxanomy terms from the CSV file.
 *
 * @throws \FieldException
 *   Field exception.
 * @throws \Exception
 *   Exception.
 */
function kupindo_add_categories() {
  kupindo_delete_old_categories();
  $categories = kupindo_load_csv_categories();
  kupindo_sort_categories($categories);
  kupindo_create_categories_taxonomy($categories);
}

/**
 * Loads the CSV file.
 *
 * @return array
 *   CSV lines.
 */
function kupindo_load_csv_categories() {

  $file = file(drupal_get_path('module', 'kupindo_xml') . '/categories.csv');

  $csv = array_map('str_getcsv', $file, array_fill(0, count($file), ';'));
  $headers = array_shift($csv);
  $rows = [];

  foreach ($csv as $row) {
    $rows[] = array_combine($headers, $row);
  }

  return $rows;
}

/**
 * Creates categories from the CSV.
 *
 * @param array $categories
 *   List of categories.
 *
 * @throws \FieldException
 *   Field exception.
 */
function kupindo_create_categories_taxonomy(array $categories) {
  // Create a vocabulary.
  $vocabulary = (object) [
    'name' => t('Kupindo categories'),
    'description' => t('Vocabulary of Kupindo categories'),
    'machine_name' => 'kupindo_categories',
  ];
  taxonomy_vocabulary_save($vocabulary);
  // Create a field.
  $kupindo_internal_id = [
    'field_name' => 'field_kupindo_internal_id',
    'cardinality' => 1,
    'type' => 'number_integer',
  ];
  field_create_field($kupindo_internal_id);
  // Create field instance.
  $kupindo_internal_id_instance = [
    'field_name' => 'field_kupindo_internal_id',
    'label' => t('Kupindo category ID'),
    'entity_type' => t('taxonomy_term'),
    'bundle' => 'kupindo_categories',
    'widget' => [
      'type' => 'number',
    ],
  ];
  field_create_instance($kupindo_internal_id_instance);
  // Setup term creation batch.
  kupindo_create_category_terms(
    $categories,
    (int) taxonomy_vocabulary_machine_name_load('kupindo_categories')->vid
  );
}

/**
 * Create term from category.
 *
 * @param array $categories
 *   Array of categories to create.
 * @param int $vid
 *   Category vocabulary ID.
 */
function kupindo_create_category_terms(array $categories, $vid) {
  // Create batch.
  $batch = [
    'title' => t('Kupindo XML Installation'),
    'init_message' => t('Starting...'),
    'progress_message' => t('Created @current terms out of @total.'),
    'error_message' => t('An error has occurred.'),
    'operations' => [],
    'finished' => '_kupindo_category_terms_batch_finished',
  ];

  // Start looping.
  foreach ($categories as $category) {
    $kupindo_id = (int) $category['IDKategorija'];
    $parent_id = (int) $category['IDNadKategorija'];
    $name = $category['Naziv'];
    $parent_name = isset($category['Pozicija']) ? $category['Pozicija'] : $name;

    $batch['operations'][] = [
      'kupindo_category_terms_batch_operation',
      [
        $kupindo_id,
        $parent_id,
        $name,
        $parent_name,
        $vid,
        count($categories),
      ],
    ];
  }

  // Start batch.
  batch_set($batch);
}

/**
 * Batch operation.
 *
 * @param int $kupindo_id
 *   Kupindo internal ID.
 * @param int $parent_id
 *   Kupindo category parent ID.
 * @param string $name
 *   Category name.
 * @param string $parent_name
 *   Category parent name.
 * @param int $vid
 *   Vocabulary ID.
 * @param int $max
 *   Number of categories.
 * @param array $context
 *   Status information.
 */
function kupindo_category_terms_batch_operation($kupindo_id, $parent_id, $name, $parent_name, $vid, $max, array &$context) {
  // Check if first operation.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
  }
  // Check if term exists.
  $term = taxonomy_get_term_by_name($name);
  if (count($term) === 0) {
    // Create a term.
    if ($parent_name === $name) {
      $term = kupindo_create_term($name, $vid, $kupindo_id);
      taxonomy_term_save($term);
    }
    elseif (strpos($parent_name, '/') !== FALSE) {
      kupindo_create_parent($kupindo_id, $parent_id, $parent_name, $vid);
    }
  }

  $context['sandbox']['progress']++;
}

/**
 * Create term and it's parents recursively.
 *
 * @param int $kupindo_id
 *   Base Kupindo ID.
 * @param int $parent_id
 *   Base parent ID.
 * @param string $parent_name
 *   Base parent name.
 * @param int $vid
 *   Vocabulary ID.
 */
function kupindo_create_parent($kupindo_id, $parent_id, $parent_name, $vid) {
  // Check if parent exists.
  $parent_array = explode('/', $parent_name);
  // Start creating parents.
  for ($i = 0; $i < count($parent_array) - 1; $i++) {
    $parent_name = $parent_array[$i];
    $child_name = $parent_array[$i + 1];
    $parent = taxonomy_get_term_by_name($parent_name);
    if (count($parent) === 0) {
      $parent = kupindo_create_term($parent_name, $vid, $parent_id);
      taxonomy_term_save($parent);
      $parent = taxonomy_get_term_by_name($parent_name);
    }
    $parent = reset($parent);
    $parent_id = (int) $parent->tid;
    // Only create the child if you need to.
    $child = taxonomy_get_term_by_name($child_name);
    if (count($child) === 0) {
      $term = kupindo_create_term($child_name, $vid, $kupindo_id, $parent_id);
      taxonomy_term_save($term);
    }
  }
}

/**
 * Create a taxonomy term.
 *
 * @param string $name
 *   Term name.
 * @param int $vid
 *   Kupindo vocabulary ID.
 * @param int $kupindo_id
 *   Kupindo ID.
 * @param int $parent_id
 *   Term parent ID.
 *
 * @return object
 *   Taxonomy term object.
 */
function kupindo_create_term($name, $vid, $kupindo_id, $parent_id = 0) {
  $term = (object) [
    'name' => trim($name),
    'description' => '',
    'vid' => $vid,
    'parent' => $parent_id,
  ];
  $term->field_kupindo_internal_id[LANGUAGE_NONE][0]['value'] = $kupindo_id;
  return $term;
}

/**
 * Delete the old category taxonomy.
 *
 * @throws \Exception
 *   Exception.
 */
function kupindo_delete_old_categories() {
  $kupindo_taxonomy = taxonomy_vocabulary_machine_name_load('kupindo_categories');
  if ($kupindo_taxonomy !== FALSE) {
    taxonomy_vocabulary_delete($kupindo_taxonomy->vid);
    field_delete_field('field_kupindo_internal_id');
    field_purge_batch(1);
  }
}

/**
 * Sort Kupindo categories.
 *
 * @param array $categories
 *   Array of categories.
 */
function kupindo_sort_categories(array &$categories) {
  usort($categories, function ($a, $b) {
    if (isset($a['IDKategorija']) && isset($b['IDKategorija'])) {
      return $a['IDKategorija'] > $b['IDKategorija'];
    }
    elseif (isset($a['IDKategorija']) && !isset($b['IDKategorija'])) {
      return TRUE;
    }
    elseif (!isset($a['IDKategorija']) && isset($b['IDKategorija'])) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  });
}

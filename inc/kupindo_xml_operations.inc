<?php

require_once  __DIR__ . '/Array2XML.inc';

/**
 * Export products and save them in an XML file.
 *
 * @return array
 *   Render array containing an URL to the file.
 * @throws \Exception
 *   Exception.
 */
function kupindo_xml_export() {
  // Load all set nodes.
  drupal_page_is_cacheable(FALSE);
  $nids = db_query('SELECT entity_id FROM `field_data_kupindo_for_kupindo` WHERE kupindo_for_kupindo_value = 1')->fetchCol(0);
  // Starts creating the XML.
  $Prodavnica = [
    'PredmetiLista' => [
        '@attributes' => [
          'type' => 'array',
        ],
    ],
  ];

  foreach ($nids as $i => $nid) {
    // Load node.
    $node = node_load($nid);
    // Check if it has all fields.
    if (kupindo_has_all_fields($node)) {
      $shipping = $node->kupindo_shipping_type[LANGUAGE_NONE];
      $payment = $node->kupindo_payment_type[LANGUAGE_NONE];
      $product_state = isset($node->kupindo_product_state[LANGUAGE_NONE][0]['value']) ? $node->kupindo_product_state[LANGUAGE_NONE][0]['value'] : 0;
      $msg = kupindo_is_shipping_valid($shipping, $payment, $node->title);
      if ($msg !== FALSE) {
        drupal_set_message($msg, 'error');
        continue;
      }

      $term = taxonomy_term_load($node->kupindo_subcategory_id[LANGUAGE_NONE][0]['tid']);
      $characteristics = kupindo_get_characteristics($node->field_product[LANGUAGE_NONE]);

      $Prodavnica['PredmetiLista']['Predmet'][] = [
        'Aktivan' => $node->kupindo_active_product[LANGUAGE_NONE][0]['value'],
        'IDPodKategorija' => $term->field_kupindo_internal_id[LANGUAGE_NONE][0]['value'],
        'Naziv' => [
          '@cdata' => $node->title,
        ],
        'Opis' => [
          '@cdata' => strip_tags(preg_replace('#<br\s*/?>#i', "\n", nl2br($node->body[LANGUAGE_NONE][0]['safe_value']))),
        ],
        'Cena' => isset($characteristics[0]['Cena']) ? $characteristics[0]['Cena'] : 0,
        'Kolicina' => isset($node->kupindo_kolicina[LANGUAGE_NONE][0]['value']) ? $node->kupindo_kolicina[LANGUAGE_NONE][0]['value'] : 0,
        'IDInterni' => $nid,
        'GTIN' => '',
        'Stanje' => $product_state,
        'NacinSlanja' => [
          'Slanje' => kupindo_get_values($shipping),
          '@attributes' => [
            'type' => 'array',
          ],
        ],
        'NacinPlacanja' => [
          'Placanje' => kupindo_get_values($payment),
          '@attributes' => [
            'type' => 'array',
          ],
        ],
        'BesplatnaDostava' => $node->kupindo_free_shipping[LANGUAGE_NONE][0]['value'],
        'YoutubeURL' => NULL,
        'Garancija' => $node->kupindo_warranty[LANGUAGE_NONE][0]['value'],
        'DuzinaGarancije' => isset($node->kupindo_warranty[LANGUAGE_NONE][0]) && (int) $node->kupindo_warranty[LANGUAGE_NONE][0]['value'] === 1 ? 1 : NULL,
        'Karakteristike' => [
          'Karakteristika' => $characteristics,
          '@attributes' => [
            'type' => 'array',
          ],
        ]
      ];

    }
    else {
      continue;
    }
  }
  $url = 'public://kupindo-xml.xml';
  $xml = Array2XML::createXML('Prodavnica',$Prodavnica);
  $xml->save($url);
  return [
      '#markup' => (t('Your Kupindo XML has been created and is located at: !url', ['!url' => '<a href="' . file_create_url($url) . '">' . file_create_url($url) . '</a>']))
    ];
}

/**
 * Fetch value from $arr.
 *
 * @param array $arr
 *   Array.
 * @param string|int $i
 *   Index.
 *
 * @return array
 *   Value of arrays.
 */
function kupindo_get_values($arr, $i = 'value') {
  $s = [];
  foreach($arr as $k => $v) {
    $s[] = $v[$i];
  }
  return $s;
}

/**
 * Get stock.
 */
function kupindo_get_stock($stock) {
  return intval($stock) < -1 ? 0 : $stock;
}

/**
 * Get images from variations
 */
function kupindo_get_variation_images($product) {
  $images = [];

  if (isset($product->field_image)) {
    foreach ($product->field_image as $field_images) {
      foreach ($field_images as $key => $value) {
        $images[] = file_create_url($value['uri']);
      }
    }
  }

  if (isset($product->field_image_cache)) {
    foreach ($product->field_image_cache as $field_images) {
      foreach ($field_images as $key => $value) {
        $images[] = file_create_url($value['uri']);
      }
    }
  }

  return $images;
}

/**
 * Get characteristics from variations.
 */
function kupindo_get_characteristics($pids) {

  $characteristics = [];

  foreach ($pids as $pid) {
    $pid = $pid['product_id'];
    $product = commerce_product_load($pid);

    $fields = '';
    foreach ((array) $product as $key => $value) {
      if (strpos($key, 'field') !== FALSE && strpos($key,'commerce') === FALSE) {
        // Fetch the taxonomy key
        $key = field_info_instance('commerce_product', $key, 'product')['label'];
        if (empty($key)) {
          continue;
        }
        elseif (isset($value[LANGUAGE_NONE][0])) {
          if (isset($value[LANGUAGE_NONE][0]['value'])) {
            $value = $value[LANGUAGE_NONE][0]['value'];
          }
          else {
            $value = taxonomy_term_load(reset($value[LANGUAGE_NONE][0]));
            if (!$value) {
              continue;
            }
            $value = $value->name;
          }
          $fields .= "$key: $value;";
        }
      }
    }
    $fields = rtrim($fields,';');
    if (empty($fields)) {
      $fields .= t('Stock: @stock', [
        '@stock' => kupindo_try_get_stock($product),
      ]);
    }

    $characteristics[] = [
      'Nazivi' => $fields,
      'Cena' => ((float) commerce_product_calculate_sell_price($product)['amount']) / 100,
      'Kolicina' => kupindo_try_get_stock($product),
      'Images' => [
        'ImageURL' => kupindo_get_variation_images($product),
        '@attributes' => [
          'type' => 'array',
        ],
      ],
    ];

  }

  return $characteristics;
}

/**
 * Get stock.
 */
function kupindo_try_get_stock($product) {
  return isset($product->commerce_stock[LANGUAGE_NONE][0]['value']) ? (int) kupindo_get_stock($product->commerce_stock[LANGUAGE_NONE][0]['value']) : 0;
}

/**
 * Check if the node has all required fields.
 */
function kupindo_has_all_fields($product) {
  $res = isset($product->kupindo_subcategory_id[LANGUAGE_NONE][0])
         && isset($product->kupindo_product_state[LANGUAGE_NONE][0])
         && isset($product->kupindo_shipping_type[LANGUAGE_NONE][0])
         && isset($product->kupindo_payment_type[LANGUAGE_NONE][0])
         && isset($product->kupindo_free_shipping[LANGUAGE_NONE][0]);
  if (!$res) {
    drupal_set_message(t('The specified product @title doesn\'t have all required fields', [ '@title' => $product->title ]),'error');
  }
  return $res;
}

/**
 * Check if the shipping and payment information is valid.
 */
function kupindo_is_shipping_valid($shipping, $payment, $title) {
  $shipping_agency = [1, 2, 3, 4, 5, 6];
  $payment_type = [4, 6];
  foreach ($shipping as $key => $value) {
    $s_val = $value['value'];
    if(in_array($s_val, $shipping_agency)) {
      $payment_has_personal = FALSE;
      foreach ($payment as $p_key => $p_value) {
        $p_val = $p_value['value'];
        if(in_array($p_val, $payment_type)){
          $payment_has_personal = TRUE;
          break;
        }
      }
      if(!$payment_has_personal){
        return t('If you chose shipping via a courier you must choose \'Plaćanje pouzećem\' or \'Tekući racun\'. Product: @title', [ '@title' => $title ]);
      }
    }
  }

  foreach ($payment as $key => $value) {
    $p_val = $value['value'];
    if(in_array($p_val, $payment_type)) {
      $shipping_has_personal = FALSE;
      foreach ($shipping as $p_key => $p_value) {
        $s_val = $p_value['value'];
        if(in_array($s_val, $shipping_agency)){
          $shipping_has_personal = TRUE;
          break;
        }
      }
      if(!$shipping_has_personal){
        return t('If you chose payment types \'Plaćanje pouzećem\' or \'Tekući racun\' you must choose a shipping courier. Product: @title', [ '@title' => $title ]);;
      }
    }
  }
  return FALSE;
}

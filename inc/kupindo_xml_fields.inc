<?php

/**
 * Creates the fields.
 */
function kupindo_xml_create_fields() {
  kupindo_delete_old_fields();
  foreach(_kupindo_get_fields() as $key => $field) {
    field_create_field($field);
    if (!(field_info_instance('node',$field['field_name'],'product_display'))) {
      $instance = array(
        'field_name' => $field['field_name'],
        'entity_type' => 'node',
        'label' => $field['label'],
        'bundle' => 'product_display',
        'required' => FALSE,
        'settings' => array(),
        'widget' => array(
          'type' => $field['widget']
        )
      );
      $instance['widget'] = is_string($field['widget']) ? array('type' => $field['widget']) : $field['widget'];
      field_create_instance($instance);
    }
  }
}

function kupindo_delete_old_fields() {
  foreach(_kupindo_get_fields() as $key => $field) {
    field_delete_field($field);
  }
}

/**
 * Get field info.
 */
function _kupindo_get_fields() {
  return array(
    array(
      'field_name' => 'kupindo_kolicina',
      'type' => 'text',
      'cardinality' => 1,
      'label' => t('Kupindo Stock'),
      'widget' => 'text_textfield',
      'description' => t('Product stock that should be displayed on Kupindo'),
    ),
    array(
      'field_name' => 'kupindo_for_kupindo',
      'type' => 'list_boolean',
      'cardinality' => 1,
      'label' => t('For Kupindo'),
      'widget' => array(
        'type' => 'options_onoff',
        'settings' => array(
          'display_label' => TRUE
        )),
      'settings'    => array(
        'allowed_values' => array(
          'Ne',
          'Da',
        )),
      'description' => t('Check if the product should be in the XML file'),
    ),
    array(
      'field_name' => 'kupindo_active_product',
      'type' => 'list_boolean',
      'cardinality' => 1,
      'label' => t('Active'),
      'widget' => array(
        'type' => 'options_onoff',
        'settings' => array(
          'display_label' => TRUE
        )),
      'settings'    => array(
        'allowed_values' => array(
          'Ne',
          'Da',
        )),
      'description' => t('Check if the product should be active on Kupindo'),
    ),
    array(
      'field_name' => 'kupindo_subcategory_id',
      'type' => 'taxonomy_term_reference',
      'cardinality' => 1,
      'label' => t('Kupindo Category'),
      'widget' => 'options_select',
      'description' => t('Choose the ID of the Kupindo category'),
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => 'kupindo_categories',
            'parent' => 0
          ),
        )
      )
    ),
    array(
      'field_name' => 'kupindo_product_state',
      'type' => 'list_text',
      'cardinality' => 1,
      'label' => t('Product state'),
      'widget' => 'options_select',
      'description' => t('Choose the Kupindo state of this product'),
      'settings' => array(
        'allowed_values' => array(
          1 => 'Nekorišćen',
          2 => 'Nekorišćen sa felerom',
          3 => 'Polovan bez oštećenja',
          4 => 'Polovan sa vidljivim znacima korišćenja',
          5 => 'Neispravno'
        ),
      ),
    ),
    array(
      'field_name' => 'kupindo_shipping_type',
      'type' => 'list_text',
      'cardinality' => 8,
      'label' => t('Shipping'),
      'widget' => 'options_buttons',
      'description' => t('Choose the shipping type'),
      'settings' => array(
        'allowed_values' => array(
          1 => 'AKS',
          2 => 'BEX',
          3 => 'City Express',
          4 => 'Pošta',
          5 => 'Post Express',
          6 => 'DExpress',
          7 => 'Lično preuzimanje',
          8 => 'Organizovani transport'
        )
      )
    ),
    array(
      'field_name' => 'kupindo_payment_type',
      'type' => 'list_text',
      'cardinality' => 5,
      'label' => t('Payment'),
      'widget' => 'options_buttons',
      'description' => t('Choose the payment type'),
      'settings' => array(
        'allowed_values' => array(
          4 => 'Plaćanje pouzećem',
          5 => 'Lično',
          6 => 'Tekući račun (pre slanja)',
          7 => 'PostNet (pre slanja)',
          8 => 'Ostalo (pre slanja)'
        )
      )
    ),
    array(
      'field_name' => 'kupindo_free_shipping',
      'type' => 'list_boolean',
      'cardinality' => 1,
      'label' =>  t('Free shipping'),
      'widget' => array(
        'type' => 'options_onoff',
        'settings' => array(
          'display_label' => TRUE
        )),
      'settings'    => array(
        'allowed_values' => array(
          'Ne',
          'Da',
        )),
      'description' => t('Check if the product has free shipping')
    ),
    array(
      'field_name' => 'kupindo_warranty',
      'type' => 'list_boolean',
      'cardinality' => 1,
      'label' => t('Warranty'),
      'widget' => array(
        'type' => 'options_onoff',
        'settings' => array(
          'display_label' => TRUE
        )),
      'settings'    => array(
        'allowed_values' => array(
          'Ne',
          'Da',
        )),
      'description' => t('Check if the product has warranty')
    ),
    array(
      'field_name' => 'kupindo_warranty_length',
      'type' => 'text',
      'cardinality' => 1,
      'label' => t('Warranty length'),
      'widget' => 'text_textfield',
      'description' => t('Enter the warranty length')
    ),
  );
}

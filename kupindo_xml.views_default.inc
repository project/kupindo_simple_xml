<?php

/**
 * @file
 * View declaration.
 */

/**
 * {@inheritdoc}
 */
function kupindo_xml_views_default_views() {
  $views = [];

  $view = new view();
  $view->name = 'kupindo_products';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Kupindo Products';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = t('Kupindo Products');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = t('More');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer nodes';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = t('Submit');
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = t('Reset');
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = t('Sort by');
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = t('Ascending');
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = t('Descending');
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = t('Items per page');
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = t('- All -');
  $handler->display->display_options['pager']['options']['tags']['first'] = t('« First');
  $handler->display->display_options['pager']['options']['tags']['previous'] = t('‹ Previous');
  $handler->display->display_options['pager']['options']['tags']['next'] = t('Next ›');
  $handler->display->display_options['pager']['options']['tags']['last'] = t('Last »');
  $handler->display->display_options['style_plugin'] = 'table_megarows';
  $handler->display->display_options['style_options']['loading_text'] = t('Loading...');
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Sadržaj: Kupindo Category (kupindo_subcategory_id) */
  $handler->display->display_options['relationships']['kupindo_subcategory_id_tid']['id'] = 'kupindo_subcategory_id_tid';
  $handler->display->display_options['relationships']['kupindo_subcategory_id_tid']['table'] = 'field_data_kupindo_subcategory_id';
  $handler->display->display_options['relationships']['kupindo_subcategory_id_tid']['field'] = 'kupindo_subcategory_id_tid';
  $handler->display->display_options['relationships']['kupindo_subcategory_id_tid']['label'] = t('Kupindo Category');
  $handler->display->display_options['relationships']['kupindo_subcategory_id_tid']['required'] = TRUE;
  /* Relationship: Sadržaj: Referenced products */
  $handler->display->display_options['relationships']['field_product_product_id']['id'] = 'field_product_product_id';
  $handler->display->display_options['relationships']['field_product_product_id']['table'] = 'field_data_field_product';
  $handler->display->display_options['relationships']['field_product_product_id']['field'] = 'field_product_product_id';
  $handler->display->display_options['relationships']['field_product_product_id']['label'] = t('Referenced products');
  $handler->display->display_options['relationships']['field_product_product_id']['required'] = TRUE;
  /* Field: Sadržaj: Naslov */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = t('Title');
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
  /* Field: Field: Galerija */
  $handler->display->display_options['fields']['field_image_1']['id'] = 'field_image_1';
  $handler->display->display_options['fields']['field_image_1']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image_1']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image_1']['label'] = t('Image');
  $handler->display->display_options['fields']['field_image_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_1']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_image_1']['delta_offset'] = '0';
  /* Field: Sadržaj: Kupindo Category */
  $handler->display->display_options['fields']['kupindo_subcategory_id']['id'] = 'kupindo_subcategory_id';
  $handler->display->display_options['fields']['kupindo_subcategory_id']['table'] = 'field_data_kupindo_subcategory_id';
  $handler->display->display_options['fields']['kupindo_subcategory_id']['field'] = 'kupindo_subcategory_id';
  /* Sort criterion: Sadržaj: Datum objavljivanja */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Sort criterion: Commerce Product: Datum kreiranja */
  $handler->display->display_options['sorts']['created_1']['id'] = 'created_1';
  $handler->display->display_options['sorts']['created_1']['table'] = 'commerce_product';
  $handler->display->display_options['sorts']['created_1']['field'] = 'created';
  $handler->display->display_options['sorts']['created_1']['relationship'] = 'field_product_product_id';
  $handler->display->display_options['sorts']['created_1']['order'] = 'DESC';
  /* Filter criterion: Sadržaj: Objavljeno */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/settings/kupindo-xml';

  $views[$view->name] = $view;

  return $views;
}
